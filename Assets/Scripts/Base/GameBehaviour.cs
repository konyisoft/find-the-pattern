using UnityEngine;

namespace Konyisoft
{
	public class GameBehaviour : MonoBehaviour
	{
		#region Properties

		public Collider Collider
		{
			get; private set;
		}

		public Renderer Renderer
		{
			get; private set;
		}

		public Transform Transform
		{
			get; private set;
		}

		public bool HasCollider
		{
			get { return Collider != null; }
		}

		public bool HasRenderer
		{
			get { return Renderer != null; }
		}
		
		public bool HasMaterial
		{
			get { return HasRenderer && Renderer.material != null; }
		}

		#endregion

		#region Mono methods

		protected virtual void Awake()
		{
			Collider = GetComponent<Collider>();
			Renderer = GetComponent<Renderer>();
			Transform = GetComponent<Transform>();
		}

		#endregion
	}
}
