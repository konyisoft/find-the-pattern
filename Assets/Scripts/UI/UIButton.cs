using UnityEngine;

namespace Konyisoft
{
	public class UIButton : GameBehaviour
	{
		#region Fields
		
		public bool clickable = true;
		public TextMesh textMesh;
		public Texture2D buttonUpTexture;
		public Color buttonUpColor = Color.white;
		public Texture2D buttonDownTexture;
		public Color buttonDownColor = Color.white;
		
		public delegate void OnClick();
		public OnClick onClick;
		
		RaycastHit? firstHit = null;
		int fingerId = -1;
		
		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			if (textMesh == null)
				textMesh = GetComponentInChildren<TextMesh>();
			Default();
		}
		
		void Update()
		{
			if (!clickable)
				return;
				
			#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBGL
				Mouse();
			#endif
			
			#if !UNITY_EDITOR && (UNITY_ANDROID || UNITY_IPHONE)
				Touch();
			#endif				
		}
		
		#endregion
		
		#region Private methods
		
		void SetTexture(Texture2D texture)
		{
			if (HasMaterial && texture != null)
				Renderer.material.mainTexture = texture;
		}
		
		void SetTextColor(Color color)
		{
			if (textMesh != null)
				textMesh.color = color;
		}
		
		void Default()
		{
			SetTexture(buttonUpTexture);
			SetTextColor(buttonUpColor);
		}

		RaycastHit? Raycast(Vector2 position)
		{
			return InputManager.Instance.Raycast(position, this, Constants.LayerMasks.Scene);
		}
	
		void Mouse()
		{
			// Mouse down
			if (Input.GetMouseButtonDown(0))
			{
				RaycastHit? hit = Raycast(Input.mousePosition);
				if (hit.HasValue)
				{
					firstHit = hit;
					BeginClick();
				}
			}
			
			// Mouse up
			if (firstHit.HasValue && Input.GetMouseButtonUp(0))
			{
				RaycastHit? hit = Raycast(Input.mousePosition);
				if (hit.HasValue && hit.Value.collider == firstHit.Value.collider)
					DoClick();
				CancelClick();
			}
		}

		void Touch()
		{
			if (Input.touchCount > 0)
			{
				// Looking for a touch began
				if (InputManager.Instance.HasBeganTouch())
				{
					Touch? touch = InputManager.Instance.GetLastBeganTouch();
					if (touch.HasValue)
						fingerId = touch.Value.fingerId;
				}
				
				// Handle the touch
				if (fingerId > -1)
				{
					Touch? touch = InputManager.Instance.GetTouchByFingerId(fingerId);
					if (touch.HasValue)
					{
						RaycastHit? hit = null;
						switch (touch.Value.phase)
						{
							// Began
							case TouchPhase.Began :
								hit = Raycast(touch.Value.position);
								if (hit.HasValue)
								{
									firstHit = hit;
									BeginClick();
								}
							break;

							// Ended
							case TouchPhase.Ended :
								hit = Raycast(touch.Value.position);
								if (hit.HasValue && hit.Value.collider == firstHit.Value.collider)
									DoClick();
								CancelClick();
								break;
							
							// Canceled
							case TouchPhase.Canceled :
								CancelClick();
								break;
						}
					}
					else
					{
						CancelClick();
					}
				}
			}
			// No touch
			else if (firstHit.HasValue)
			{
				CancelClick();
			}
		}

		void BeginClick()
		{
			SetTexture(buttonDownTexture);
			SetTextColor(buttonDownColor);
		}
		
		void CancelClick()
		{
			Default();
			firstHit = null;
			fingerId = -1;
		}
		
		void DoClick()
		{
			if (onClick != null)
				onClick();
		}

		#endregion	
	}
}
