using UnityEngine;

public static class Utils
{
	public static Vector2 Vector2D(Vector3 v)
	{
		return new Vector2(v.x, v.y);
	}
}
