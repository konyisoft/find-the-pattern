using UnityEngine;
using System.Collections;

namespace Konyisoft
{
	public class Symbol : GameBehaviour
	{
		#region Fields

		public float scaleSpeed = 1f;
		public float fadeInTime = 1f;
		public float fadeOutTime = 1f;
		public float upTime = 1f;

		Vector3 origScale;
		bool isFading = false;

		#endregion

		#region Properties

		public bool IsFading
		{
			get { return isFading; }
		}

		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			origScale = Transform.localScale;
			Hide();
		}

		void Update()
		{
			Vector3 scale = Transform.localScale;
			scale.x += Time.deltaTime * scaleSpeed;
			scale.y = scale.x;
			Transform.localScale = scale;
		}

		#endregion

		#region Public methods

		public void Show()
		{
			Default();
			SetActive(true);
			StartCoroutine(CO_DoFadingProcess());
		}

		public void Hide()
		{
			Default();
			SetActive(false);
		}

		public void Default()
		{
			StopAllCoroutines();
			SetAlpha(0f);
			isFading = false;
			Transform.localScale = origScale;
		}

		#endregion

		#region Private methods

		void SetActive(bool active)
		{
			gameObject.SetActive(active);
		}

		void SetAlpha(float alpha)
		{
			if (HasMaterial)
			{
				Color c = Renderer.material.color;
				c.a = alpha;
				Renderer.material.color = c;
			}
		}

		IEnumerator CO_DoFadingProcess()
		{
			isFading = true;
			yield return StartCoroutine(CO_Fade(0f, 1f, fadeInTime));
			yield return new WaitForSeconds(upTime);
			yield return StartCoroutine(CO_Fade(1f, 0f, fadeOutTime));
			isFading = false;
		}

		IEnumerator CO_Fade(float fromAlpha, float toAlpha, float duration)
		{
			float startTime = Time.time;
			SetAlpha(fromAlpha);
			while (Time.time - startTime < duration)
			{
				SetAlpha(Mathf.Lerp(fromAlpha, toAlpha, (Time.time - startTime) / duration));
				yield return null;
			}
			SetAlpha(toAlpha);
			yield return null;
		}

		#endregion
	}
}
