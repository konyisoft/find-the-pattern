using UnityEngine;

namespace Konyisoft
{
	public class Clickable : GameBehaviour
	{
		#region Properties

		public bool Visible
		{
			get; private set;
		}

		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			SetVisible(false);
		}

		#endregion

		#region Public methods

		public void Show()
		{
			SetVisible(true);
		}

		public void Hide()
		{
			SetVisible(false);
		}

		public void SetSize(Vector2 size)
		{
			Transform.localScale = new Vector3(size.x, size.y, Transform.localScale.z);
		}

		public void SetPosition(Vector2 position)
		{
			Transform.position = new Vector3(position.x, position.y, Transform.position.z);
		}

		public RaycastHit? Raycast(Vector2 position)
		{
			return InputManager.Instance.Raycast(position, this, Constants.LayerMasks.Tools);
		}

		#endregion

		#region Private methods

		void SetVisible(bool b)
		{
			Renderer.enabled = b;
			Visible = b;
		}

		#endregion
	}
}
