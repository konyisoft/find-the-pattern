using UnityEngine;

namespace Konyisoft
{
	public class Marker : GameBehaviour
	{
		#region Classes and structs

		[System.Serializable]
		public struct Constraints
		{
			public float minX;
			public float maxX;
			public float minY;
			public float maxY;
		}

		#endregion

		#region Fields

		public float duration = 1.0f;
		public Color startColor = Color.white;
		public Color endColor = Color.blue;
		[Range(1, 100)]
		public int cols = 5;
		[Range(1, 100)]
		public int rows = 5;
		public bool movable = false;
		public float lerpSpeed = 15f;
		public Constraints constraints;

		bool isFlashing = true;
		bool isLerping = false;
		RaycastHit? firstHit = null;
		int fingerId = -1;
		Vector3 localHitPoint = Vector2.zero;
		Cell currentCell = null;
		bool visible = false;
		float time = 0.0f;
		Vector2 currentLeftTopPosition = Vector2.zero;
		Vector2 desiredLeftTopPosition = Vector2.zero;

		#endregion

		#region Properties

		public bool IsFlashing
		{
			get { return isFlashing; }
			set { SetFlashing(value); }
		}

		public bool Visible
		{
			get { return visible; }
		}

		public Vector2 LeftTopPosition
		{
			get { return GetLeftTopPosition(); }
		}

		public Cell Cell
		{
			get { return currentCell; }
		}

		public bool HasCell
		{
			get { return currentCell != null; }
		}

		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			SetVisible(visible);
			SetFlashing(isFlashing);
			Resize();
		}

		void Update()
		{
			// Do flash
			if (isFlashing)
			{
				time += Time.deltaTime;
				float t = Mathf.PingPong(time, duration) / duration;
				SetColor(Color.Lerp(startColor, endColor, t));
			}

			if (isLerping)
			{
				currentLeftTopPosition = Vector2.MoveTowards(currentLeftTopPosition, desiredLeftTopPosition, Time.deltaTime * lerpSpeed);
				SetLeftTopPosition(currentLeftTopPosition);
				float distance = (desiredLeftTopPosition - currentLeftTopPosition).sqrMagnitude;
				// Lerping ends
				if (distance < 0.01f)
				{
					currentLeftTopPosition = desiredLeftTopPosition;
					SetLeftTopPosition(currentLeftTopPosition);
					isLerping = false;
				}
			}

			// Input won't work if no table generated
			if (!movable || !TableManager.Instance.IsGenerated || isLerping)
			{
				if (firstHit.HasValue)
					CancelClick();
				return;
			}

#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBGL
			Mouse();
#endif

#if !UNITY_EDITOR && (UNITY_ANDROID || UNITY_IPHONE)
			Touch();
#endif
		}

		#endregion

		#region Public methods

		public void SetFlashing(bool b)
		{
			isFlashing = b;
			if (!b)
				ResetColor();
		}

		public void Show()
		{
			SetVisible(true);
		}

		public void Hide()
		{
			SetVisible(false);
		}

		public void Resize()
		{
			Transform.localScale = new Vector3(
				cols * TableManager.Instance.tableSettings.cellDistance,
				rows * TableManager.Instance.tableSettings.cellDistance,
				Transform.localScale.z
			);
		}

		public void SetSize(int cols, int rows)
		{
			this.cols = cols;
			this.rows = rows;
			Resize();
		}

		public void AlignTo(Cell cell, bool withLerp = false)
		{
			if (cell != null)
				AlignTo(cell.Col, cell.Row, withLerp);
		}

		public void AlignTo(int col, int row, bool withLerp = false)
		{
			isLerping = false;
			currentLeftTopPosition = Vector2.zero;
			desiredLeftTopPosition = Vector2.zero;

			// Range check
			if (col < 0)
				col = 0;
			if (row < 0)
				row = 0;
			if (col > TableManager.Instance.tableSettings.cols - cols)
				col = TableManager.Instance.tableSettings.cols - cols;
			if (row > TableManager.Instance.tableSettings.rows - rows)
				row = TableManager.Instance.tableSettings.rows - rows;

			Cell cell = TableManager.Instance.Table[col, row];
			if (cell != null)
			{
				float halfCellDistance = TableManager.Instance.tableSettings.cellDistance / 2f;
				Vector2 cellPosition = cell.Position2D;
				desiredLeftTopPosition = new Vector2(cellPosition.x - halfCellDistance, cellPosition.y + halfCellDistance);
				if (withLerp)
				{
					isLerping = true;
					currentLeftTopPosition = GetLeftTopPosition();
				}
				else
				{
					SetLeftTopPosition(desiredLeftTopPosition);
				}
				currentCell = cell;
			}
		}

		#endregion

		#region Private methods

		void ResetColor()
		{
			SetColor(startColor);
		}

		void SetColor(Color c)
		{
			if (Renderer != null && Renderer.material != null)
			{
				Renderer.material.color = c;
			}
		}

		void SetVisible(bool b)
		{
			gameObject.SetActive(b);
			visible = b;
			time = 0.0f;
		}

		Vector2 GetLeftTopPosition()
		{
			return GetLeftTopPosition(Transform.position);
		}

		Vector2 GetLeftTopPosition(Vector3 centerPosition)
		{
			return new Vector2(
				centerPosition.x - (Transform.localScale.x / 2f),
				centerPosition.y + (Transform.localScale.y / 2f)
			);
		}

		void SetLeftTopPosition(Vector2 position)
		{
			Transform.position = new Vector3(
				position.x + (Transform.localScale.x / 2f),
				position.y - (Transform.localScale.y / 2f),
				Transform.position.z
			);
		}

		RaycastHit? Raycast(Vector2 position)
		{
			return InputManager.Instance.Raycast(position, this, Constants.LayerMasks.Tools);
		}

		void Mouse()
		{
			// Mouse down
			if (Input.GetMouseButtonDown(0))
			{
				RaycastHit? hit = Raycast(Input.mousePosition);
				if (hit.HasValue && hit.Value.collider == Collider)
				{
					Collider.enabled = false;
					firstHit = hit;
					localHitPoint = firstHit.Value.point - Transform.position;
					currentCell = null;
				}
				else
				{
					CancelClick();
				}
			}

			// Mouse move
			if (firstHit.HasValue && Input.GetMouseButton(0))
			{
				RaycastHit? hit = ToolsManager.Instance.clickable.Raycast(Input.mousePosition);
				if (hit.HasValue)
				{
					Vector3 position = hit.Value.point - localHitPoint;
					position.x = Mathf.Clamp(position.x, constraints.minX, constraints.maxX);
					position.y = Mathf.Clamp(position.y, constraints.minY, constraints.maxY);
					Transform.position = new Vector3(position.x, position.y, Transform.position.z);
				}
			}

			// Mouse up
			if (firstHit.HasValue && Input.GetMouseButtonUp(0))
			{
				RaycastHit? hit = ToolsManager.Instance.clickable.Raycast(Input.mousePosition);
				if (hit.HasValue)
				{
					Vector3 position = hit.Value.point - localHitPoint;
					Vector3 leftTopPosition = GetLeftTopPosition(position);
					Cell cell = TableManager.Instance.GetNearestCell(leftTopPosition);
					if (cell != null)
						AlignTo(cell, true);
				}
				CancelClick();
			}
		}

		void Touch()
		{
			if (Input.touchCount > 0)
			{
				// Looking for a touch began
				if (InputManager.Instance.HasBeganTouch())
				{
					Touch? touch = InputManager.Instance.GetLastBeganTouch();
					if (touch.HasValue)
						fingerId = touch.Value.fingerId;
				}

				// Handle the touch
				if (fingerId > -1)
				{
					Touch? touch = InputManager.Instance.GetTouchByFingerId(fingerId);
					if (touch.HasValue)
					{
						RaycastHit? hit = null;
						switch (touch.Value.phase)
						{
							// Began
							case TouchPhase.Began:
								hit = Raycast(touch.Value.position);
								if (hit.HasValue && hit.Value.collider == Collider)
								{
									Collider.enabled = false;
									firstHit = hit;
									localHitPoint = firstHit.Value.point - Transform.position;
									currentCell = null;
								}
								else
								{
									CancelClick();
								}
								break;

							// Moved
							case TouchPhase.Moved:
								if (firstHit.HasValue)
								{
									// Raycast on clickable object!
									hit = ToolsManager.Instance.clickable.Raycast(touch.Value.position);
									if (hit.HasValue)
									{
										Vector3 position = hit.Value.point - localHitPoint;
										position.x = Mathf.Clamp(position.x, constraints.minX, constraints.maxX);
										position.y = Mathf.Clamp(position.y, constraints.minY, constraints.maxY);
										Transform.position = new Vector3(position.x, position.y, Transform.position.z);
									}
								}
								break;

							// Ended
							case TouchPhase.Ended:
								if (firstHit.HasValue)
								{
									hit = ToolsManager.Instance.clickable.Raycast(touch.Value.position);
									if (hit.HasValue)
									{
										Vector3 position = hit.Value.point - localHitPoint;
										Vector3 leftTopPosition = GetLeftTopPosition(position);
										Cell cell = TableManager.Instance.GetNearestCell(leftTopPosition);
										if (cell != null)
											AlignTo(cell, true);
									}
								}
								CancelClick();
								break;

							// Canceled. I don't know what it could be.
							case TouchPhase.Canceled:
								CancelClick();
								break;
						}
					}
					else
					{
						CancelClick();
					}
				}
			}
			// No touch
			else if (firstHit.HasValue)
			{
				CancelClick();
			}
		}

		void CancelClick() // or touch...
		{
			Collider.enabled = true;
			firstHit = null;
			fingerId = -1;
			localHitPoint = Vector3.zero;
		}

		#endregion
	}
}
