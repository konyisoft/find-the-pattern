using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Konyisoft
{
	public class ScreenFader : Singleton<ScreenFader>
	{
		#region Fields

		public Image image;
		public float duration = 5f;
		public bool activeAtStart = false;

		public delegate void OnScreenFadedIn();
		public delegate void OnScreenFadedOut();

		public bool IsActive { get { return image != null && image.gameObject.activeInHierarchy; } }
		public bool IsFading { get { return isFading; } }
		
		bool isFading = false;
		bool breakable = false;

		#endregion

		#region Mono menthods

		protected override void Awake()
		{
			base.Awake();
			
			if (activeAtStart)
				Show();
			else
				Hide();
		}

		#endregion

		#region Public methods

		public void Show()
		{
			StopAllCoroutines();
			SetActive(true);
			SetImageAlpha(1f);
			isFading = false;
			breakable = false;
		}

		public void Hide()
		{
			StopAllCoroutines();
			SetImageAlpha(0f);
			SetActive(false);
			isFading = false;
			breakable = false;
		}

		public void FadeIn(OnScreenFadedIn callback = null, bool breakable = false)
		{
			if (this.breakable || !isFading)
			{
				this.breakable = breakable;
				StopAllCoroutines(); // Stop current if executing
				isFading = true;
				StartCoroutine(CO_FadeIn(callback));
			}
		}

		public void FadeOut(OnScreenFadedOut callback = null, bool breakable = false)
		{
			if (this.breakable || !isFading)
			{
				this.breakable = breakable;
				StopAllCoroutines(); // Stop current if executing
				isFading = true;
				StartCoroutine(CO_FadeOut(callback));
			}
		}

		#endregion

		#region Private methods

		void SetImageAlpha(float alpha)
		{
			if (image != null)
			{
				Color c = image.color;
				c.a = alpha;
				image.color = c;
			}
		}

		float GetImageAlpha()
		{
			return image != null ? image.color.a : 0f;
		}

		IEnumerator CO_FadeIn(OnScreenFadedIn callback = null)
		{
			SetActive(true);
			SetImageAlpha(1f);
			yield return new WaitForSeconds(0.15f); // Wait some time
			yield return StartCoroutine(CO_Fade(1f, 0f, duration));
			SetActive(false); // Become disabled after fading in
			if (callback != null)
				callback();
			isFading = false;
		}

		IEnumerator CO_FadeOut(OnScreenFadedOut callback = null)
		{
			SetActive(true);
			SetImageAlpha(0f);
			yield return new WaitForSeconds(0.15f); // Wait some time
			yield return StartCoroutine(CO_Fade(0f, 1f, duration));
			if (callback != null)
				callback();
			isFading = false;
			// Remains active after fading out
		}

		IEnumerator CO_Fade(float fromAlpha, float toAlpha, float duration)
		{
			if (image != null)
			{
				float startTime = Time.time;
				SetImageAlpha(fromAlpha);
				while (Time.time - startTime < duration)
				{
					SetImageAlpha(Mathf.Lerp(fromAlpha, toAlpha, (Time.time - startTime) / duration));
					yield return null;
				}
				SetImageAlpha(toAlpha);
			}
			yield return null;
		}

		void SetActive(bool active)
		{
			if (image != null)
			{
				image.gameObject.SetActive(active);
			}
		}

		#endregion
	}
}
