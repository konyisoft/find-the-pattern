using UnityEngine;
using UnityEngine.SceneManagement;

namespace Konyisoft
{
	public class EndManager : Singleton<EndManager>
	{
		#region Fields

		public TextMesh scoreText;
		public ParticleSystem fireworks;

		#endregion
		
		#region Mono methods
		
		void Start()
		{
			WriteScoreAndTime(Globals.GameValues.Score, Globals.GameValues.TotalTime);
			ScreenFader.Instance.FadeIn();
		}
		
		void Update()
		{
			// Go to title screen
			if (Input.GetKeyDown(KeyCode.Escape))
				ScreenFader.Instance.FadeOut(OnBackToTitleScreen);

			#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBGL
				Mouse();
			#endif
			
			#if !UNITY_EDITOR && (UNITY_ANDROID || UNITY_IPHONE)
				Touch();
			#endif
		}
		
		#endregion
		
		#region Private methods
		
		void Mouse()
		{
			if (Input.GetMouseButtonUp(0))
				ScreenFader.Instance.FadeOut(OnBackToTitleScreen);		
		}

		void Touch()
		{
			if (Input.touchCount == 1 && Input.touches[0].phase == TouchPhase.Ended)
				ScreenFader.Instance.FadeOut(OnBackToTitleScreen);		
		}
		
		void WriteScoreAndTime(int score, int totalTime)
		{
			if (scoreText != null)
			{
				int minutes = Mathf.FloorToInt(totalTime / 60);
				int seconds = Mathf.FloorToInt(totalTime - minutes * 60);
				scoreText.text = string.Format(Constants.Texts.EndScoreAndTime, score, minutes, seconds);
			}
		}
		
		#endregion
		
		#region Event handlers

		void OnBackToTitleScreen()
		{
			SceneManager.LoadScene(Constants.Scenes.Title);
		}		
		
		#endregion
	}
}