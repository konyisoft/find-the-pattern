//#define TESTMODE
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Konyisoft
{
	public class GameManager : Singleton<GameManager>
	{
		#region Enumerations
		
		public enum GameState
		{
			None,
			Begin,
			Playing,
			Right,
			Wrong,
			Scoring,
			ShowRight,
			GameOver,
			Ended,
			FatalError
		}
		
		#endregion
		
		#region Fields and properties

#if TESTMODE
		[Range(1, 100)]
		public int startLevelIndex = 1;
#endif
		public GameState State { get { return state; } }
		GameState state = GameState.None;
#if TESTMODE
		static bool started = false;
#endif
		
		delegate void OnWaitEnded();
		
		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
#if TESTMODE
			if (!started) // Set start level only at the beginning
			{
				Globals.GameValues.LevelIndex = startLevelIndex;
				started = true;
			}
#endif			
		}
		
		void Start()
		{
			// Fade in
			ScreenFader.Instance.FadeIn();
			
			// Level delegates
			LevelManager.Instance.onLevelLoaded += OnLevelLoaded;			
			LevelManager.Instance.onLevelUnloaded += OnLevelUnloaded;			
			
			// Timer delegates
			TimerManager.Instance.onTimeChanged += OnTimeChanged;
			TimerManager.Instance.onTimeTick += OnTimeTick;
			TimerManager.Instance.onTimeElapsed += OnTimeElapsed;
			TimerManager.Instance.onTimeLerped += OnTimeLerped;
			
			StartGame(); // Starts a new game
		}
		
		void Update()
		{
			// Back to the title screen
			if (Input.GetKeyDown(KeyCode.Escape))
				ScreenFader.Instance.FadeOut(OnBackToTitleScreen);
			
#if TESTMODE
			#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBGL
				if (Input.GetKeyDown(KeyCode.O) && Globals.GameValues.LevelIndex > 1)
				{
					Globals.GameValues.LevelIndex--;
					LoadLevel(Globals.GameValues.LevelIndex);
				}
				else if (Input.GetKeyDown(KeyCode.P) && Globals.GameValues.LevelIndex < LevelManager.Instance.LevelCount)
				{
					Globals.GameValues.LevelIndex++;
					LoadLevel(Globals.GameValues.LevelIndex);
				}
				else if (Input.GetKeyDown(KeyCode.K))  // Immediate right solution
				{
					DoneRight();
				}
				else if (Input.GetKeyDown(KeyCode.C))
				{
					string fileName = "Screenshot_{0}.png";
					ScreenCapture.CaptureScreenshot(string.Format(fileName, Globals.GameValues.LevelIndex), 0);
				}
			#endif
			
			#if !UNITY_EDITOR && (UNITY_ANDROID || UNITY_IPHONE)
				if (Input.touchCount == 3 && Input.touches[2].phase == TouchPhase.Ended && Globals.Values.LevelIndex > 1)
				{
					Globals.Values.LevelIndex--;
					LoadLevel(Globals.Values.LevelIndex);
				}
				else if (Input.touchCount == 2 && Input.touches[1].phase == TouchPhase.Ended && Globals.Values.LevelIndex < LevelManager.Instance.LevelCount)
				{
					Globals.Values.LevelIndex++;
					LoadLevel(Globals.Values.LevelIndex);
				}
			#endif
#endif			
		}

		#endregion
		
		#region Public methods

		public void ResetValues()
		{
			TimerManager.Instance.Stop();
			UpdateUI();
		}		

		public void UpdateUI()
		{
			UpdateUITime();
			UpdateUIScore();
		}
		
		public void DoneButtonClick()
		{
			// Checking pattern equality
			if (ToolsManager.Instance.marker.HasCell && !TimerManager.Instance.IsLerping)
			{
				// Temporarily disable inputs
				ToolsManager.Instance.marker.movable = false;
				UIManager.Instance.doneButton.clickable = false;
				
				// Check equality
				if (TableManager.Instance.PatternEquals(ToolsManager.Instance.marker.Cell))
					DoneRight();
				else
					DoneWrong();
			}
		}

		#endregion
		
		#region Private methods
		
		void SetState(GameState state)
		{
			if (this.state != state)
				this.state = state;
		}
		
		void StartGame()
		{
			// Start game from the beginning
			SetState(GameState.Begin);
			ResetValues();					
			LoadLevel(Globals.GameValues.LevelIndex);
		}
		
		void LoadLevel(int levelIndex)
		{
			LevelManager.Instance.LoadLevel(levelIndex);		
		}
		
		void StartLevel()
		{
			// Starts the current level. A generator is essential.
			CellGenerator generator = LevelManager.Instance.CurrentLevel.GetComponent<CellGenerator>();
			if (generator != null)
			{	
				TableManager.Instance.Generate(generator);
				ToolsManager.Instance.marker.Show();
				ToolsManager.Instance.marker.AlignTo(0, 0);
				ToolsManager.Instance.marker.movable = true;
				UIManager.Instance.doneButton.clickable = true;
				TimerManager.Instance.Set(LevelManager.Instance.CurrentLevel.timeLimit);
				TimerManager.Instance.Run();
				SetState(GameState.Playing);
			}
			else
			{
				Debug.LogError("No generator found on current level object.");
				SetState(GameState.FatalError);
				Debug.Break();
			}
		}
		
		void DoneRight()
		{
			SetState(GameState.Right);
			TimerManager.Instance.Stop();

			// The table fades out
			TableManager.Instance.FadeCells(0f, 2f, OnTableCellsFadedOut);
			
			ToolsManager.Instance.wrongSymbol.Hide();
			ToolsManager.Instance.rightSymbol.Show();
		}

		void DoneWrong()
		{
			SetState(GameState.Wrong);
			// Shake the cameras. Cool effect...
			DisplayManager.Instance.ShakeCamera(DisplayManager.Instance.sceneCamera);
			DisplayManager.Instance.ShakeCamera(DisplayManager.Instance.gameCamera);
			
			TimerManager.Instance.DecreaseWith(10, true);
			
			if (!ToolsManager.Instance.wrongSymbol.IsFading)
				ToolsManager.Instance.wrongSymbol.Show();

			Continue();
		}
		
		void Continue()
		{
			SetState(GameState.Playing);
			ToolsManager.Instance.marker.movable = true;
			UIManager.Instance.doneButton.clickable = true;
		}

		void ShowRight()
		{
			SetState(GameState.ShowRight);
			TimerManager.Instance.Stop();
			UIManager.Instance.doneButton.clickable = false;

			// Darkening the table
			TableManager.Instance.ColorizeCells(0.4f, 2.5f, OnTableCellsDarkened);

			// Move marker to the correct position
			ToolsManager.Instance.marker.movable = false;
			ToolsManager.Instance.marker.AlignTo(
				TableManager.Instance.Generator.RandomCol,
				TableManager.Instance.Generator.RandomRow,
				true
			);
		}
		
		void UpdateUITime()
		{
			UIManager.Instance.WriteTime(TimerManager.Instance.Time);
		}
	
		void UpdateUIScore()
		{
			UIManager.Instance.WriteScore(Globals.GameValues.Score);
		}
		
		IEnumerator CO_Wait(float time, OnWaitEnded callback = null)
		{
			yield return new WaitForSeconds(time);
			if (callback != null)
				callback();
		}
	
		#endregion
		
		#region Event handlers and delegates
		
		void OnTimeChanged()
		{
			UpdateUITime();
			
			if (state == GameState.Scoring)
			{
				Globals.GameValues.Score++;
				UpdateUIScore();
			}
		}

		void OnTimeTick()
		{
			UpdateUITime();
		}

		void OnTimeElapsed()
		{
			ShowRight(); // Show the right solution
		}
		
		void OnTimeLerped()
		{
			if (state == GameState.Scoring)
				StartCoroutine(CO_Wait(3.25f, OnAfterScoring));
		}
		
		void OnLevelLoaded()
		{
			StartLevel();
		}

		void OnLevelUnloaded()
		{
			// Not used at the moment
		}
		
		void OnBackToTitleScreen()
		{
			SceneManager.LoadScene(Constants.Scenes.Title);
		}
		
		void OnTableCellsDarkened()
		{
			SetState(GameState.GameOver);
			StartCoroutine(CO_Wait(5f, OnAfterTableCellsDarkened));
		}
		
		void OnAfterTableCellsDarkened()
		{
			SetState(GameState.Ended);
			ScreenFader.Instance.FadeOut(OnGameOverScreen);
		}

		void OnGameOverScreen()
		{
			SceneManager.LoadScene(Constants.Scenes.GameOver);
		}

		void OnTableCellsFadedOut()
		{
			StartCoroutine(CO_Wait(1.25f, OnAfterTableCellsFadedOut));
		}
		
		void OnAfterTableCellsFadedOut()
		{
			// Start scoring
			SetState(GameState.Scoring);
			UIManager.Instance.showAlarm = false;
			Globals.GameValues.TotalTime += LevelManager.Instance.CurrentLevel.timeLimit - TimerManager.Instance.Time;
			TimerManager.Instance.lerpTime = 0.025f; // Slow down the timer's lerp
			TimerManager.Instance.DecreaseWith(TimerManager.Instance.Time, true);
		}
		
		void OnAfterScoring()
		{
			SetState(GameState.Ended);
			ScreenFader.Instance.FadeOut(OnNextLevel);
		}
		
		void OnNextLevel()
		{
			// What to do next?
			if (Globals.GameValues.LevelIndex == LevelManager.Instance.LevelCount)
			{
				SceneManager.LoadScene(Constants.Scenes.End); // No more levels: the end. Bye!
			}
			else
			{
				Globals.GameValues.LevelIndex++;
				SceneManager.LoadScene(Constants.Scenes.Intermediate); // Go to next level
			}
		}
		
		#endregion
	}
}
