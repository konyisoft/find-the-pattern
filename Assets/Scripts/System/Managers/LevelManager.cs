using UnityEngine;

namespace Konyisoft
{
	public class LevelManager : Singleton<LevelManager>
	{
		#region Fields

		public delegate void OnLevelLoaded();
		public delegate void OnLevelUnloaded();
		
		public OnLevelLoaded onLevelLoaded;		
		public OnLevelUnloaded onLevelUnloaded;		

		GameObject levelParent;
		Level currentLevel;

		#endregion

		#region Properties

		public Level CurrentLevel
		{
			get { return currentLevel; }
		}

		public bool HasLevel
		{
			get { return currentLevel != null; }
		}
		
		public int LevelCount
		{
			get { return levelParent != null ? levelParent.transform.childCount : 0; }
		}

		#endregion

		#region Mono methods
		
		protected override void Awake()
		{
			base.Awake();
			levelParent = GameObject.Find(Constants.Objects.Levels);
			
			if (levelParent == null)
			{
				Debug.LogError("No level parent object found.");
				Debug.Break();
			}
		}
		
		#endregion
		
		#region Public methods
		
		public void LoadLevel(int levelIndex)
		{
			LoadLevel(string.Format("{0:000}", levelIndex)); // With leading zeros: 00n, 0nn, nnn
		}
		
		public void LoadLevel(string levelName)
		{
			bool levelLoaded = false;
			
			UnloadLevel(); // Unload current level
			
			// Try to find level object
			GameObject levelObject = GameObject.Find(Constants.Objects.Levels + "/" + levelName);
			if (levelObject != null)
			{
				Level level = levelObject.GetComponent<Level>();
				if (level != null)
				{
					currentLevel = level;
					levelLoaded = true;
				}
			}
			
			if (!levelLoaded)
			{
				Debug.LogError("Level '" + levelName + "' not found.");
				Debug.Break();
			}
			else
			{
				if (onLevelLoaded != null)
					onLevelLoaded();
			}
		}
		
		public void UnloadLevel()
		{
			if (currentLevel != null)
			{
				currentLevel = null;
				// Call delegate
				if (onLevelUnloaded != null)
					onLevelUnloaded();
			}
		}
		
		#endregion
	}
}
