using UnityEngine;
using UnityEngine.SceneManagement;

namespace Konyisoft
{
	public class TitleManager : Singleton<TitleManager>
	{
		#region Mono methods
		
		void Start()
		{
			ScreenFader.Instance.FadeIn();
		}
		
		void Update()
		{
			// Exit game
			if (Input.GetKeyDown(KeyCode.Escape))
				ScreenFader.Instance.FadeOut(OnQuitApplication);

			#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBGL
				Mouse();
			#endif
			
			#if !UNITY_EDITOR && (UNITY_ANDROID || UNITY_IPHONE)
				Touch();
			#endif
		}
		
		#endregion
		
		#region Private methods
		
		void Mouse()
		{
			if (Input.GetMouseButtonUp(0))
				ScreenFader.Instance.FadeOut(OnStartGame);		
		}

		void Touch()
		{
			if (Input.touchCount == 1 && Input.touches[0].phase == TouchPhase.Ended)
				ScreenFader.Instance.FadeOut(OnStartGame);		
		}

		#endregion
		
		#region Event handlers

		void OnQuitApplication()
		{
			Application.Quit();
		}

		void OnStartGame()
		{
			// Set default global values
			Globals.GameValues.Reset();
			
			// Go
			SceneManager.LoadScene(Constants.Scenes.Intermediate);
		}

		#endregion
	}
}
