using UnityEngine;
using UnityEngine.SceneManagement;

namespace Konyisoft
{
	public class IntermediateManager : Singleton<IntermediateManager>
	{
		#region Fields

		public TextMesh levelText;
		public TextMesh levelTextShadow;
		public TextMesh scoreText;
		public TextMesh introText;
		
		#endregion
		
		#region Mono methods
		
		void Start()
		{
			WriteScore(Globals.GameValues.Score);
			WriteLevel(Globals.GameValues.LevelIndex);
			
			string introText = GetIntroText(Globals.GameValues.LevelIndex);
			WriteIntroText(introText);

			ScreenFader.Instance.FadeIn();
		}
		
		void Update()
		{
			// Go to title screen
			if (Input.GetKeyDown(KeyCode.Escape))
				ScreenFader.Instance.FadeOut(OnBackToTitleScreen);

			#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBGL
				Mouse();
			#endif
			
			#if !UNITY_EDITOR && (UNITY_ANDROID || UNITY_IPHONE)
				Touch();
			#endif
		}
		
		#endregion
		
		#region Private methods
		
		void Mouse()
		{
			if (Input.GetMouseButtonUp(0))
				ScreenFader.Instance.FadeOut(OnNextLevel);		
		}

		void Touch()
		{
			if (Input.touchCount == 1 && Input.touches[0].phase == TouchPhase.Ended)
				ScreenFader.Instance.FadeOut(OnNextLevel);		
		}
		
		void WriteScore(int score)
		{
			if (scoreText != null)
				scoreText.text = string.Format(Constants.Texts.IntermediateScore, score);
		}

		void WriteLevel(int levelIndex)
		{
			if (levelText != null)
				levelText.text = string.Format(Constants.Texts.IntermediateLevel, levelIndex);
				
			if (levelTextShadow != null)
				levelTextShadow.text = levelText.text;
		}

		void WriteIntroText(string text)
		{
			if (introText != null)
				introText.text = text;
		}
	
		string GetIntroText(int levelIndex)
		{
			string r = string.Empty;
			try
			{
				r = Constants.LevelTexts[levelIndex - 1]; // Level numbering starts from 1
			}
			catch
			{
				// Returns empty string
			}
			return r;
		}

		#endregion
		
		#region Event handlers

		void OnNextLevel()
		{
			SceneManager.LoadScene(Constants.Scenes.Game);
		}

		void OnBackToTitleScreen()
		{
			SceneManager.LoadScene(Constants.Scenes.Title);
		}		
		
		#endregion
	}
}
