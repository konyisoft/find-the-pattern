using UnityEngine;

namespace Konyisoft
{
	public class DisplayManager : Singleton<DisplayManager>
	{
		#region Fields

		public Camera gameCamera;
		public Camera sceneCamera;

#if UNITY_ANDROID || UNITY_IPHONE
		DeviceOrientation lastOrientation = DeviceOrientation.LandscapeLeft;
#endif

		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();

			if (gameCamera == null)
			{
				Debug.LogError("No game camera attached.");
				Debug.Break();
			}

			if (sceneCamera == null)
			{
				Debug.LogError("No scene camera attached.");
				Debug.Break();
			}

#if UNITY_ANDROID || UNITY_IPHONE
			Screen.sleepTimeout = SleepTimeout.NeverSleep;
			lastOrientation = Input.deviceOrientation;
			RotateAnchors();
#endif
		}

#if UNITY_ANDROID || UNITY_IPHONE
		private void Update()
		{
			// Checking device orientation
			if (Input.deviceOrientation != lastOrientation)
			{
				lastOrientation = Input.deviceOrientation;
				RotateAnchors();
			}
		}
#endif

		#endregion

		#region Public methods

		public void ShakeCamera(Camera camera)
		{
			if (camera != null)
			{
				Shaker shaker = camera.GetComponent<Shaker>();
				if (shaker != null)
					shaker.Shake();
			}
		}

		#endregion

		#region Private methods

#if UNITY_ANDROID || UNITY_IPHONE
		void RotateAnchors()
		{
			switch (Input.deviceOrientation)
			{
				case DeviceOrientation.Portrait:
					UIManager.Instance.RotateAnchors(90f);
					break;
					
				case DeviceOrientation.PortraitUpsideDown:
					UIManager.Instance.RotateAnchors(270f);
					break;

				case DeviceOrientation.LandscapeLeft:
					UIManager.Instance.RotateAnchors(0f);
					break;

				case DeviceOrientation.LandscapeRight:
					UIManager.Instance.RotateAnchors(180f);
					break;
			}
		}
#endif

		#endregion
	}
}
