using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Konyisoft
{
	public class GameOverManager : Singleton<GameOverManager>
	{
		#region Fields

		public Camera sceneCamera;
		public TextMesh scoreText;

		#endregion

		#region Mono methods

		void Start()
		{
			WriteScore(Globals.GameValues.Score);
			ScreenFader.Instance.FadeIn();
			StartCoroutine(CO_ShakeCamera());
		}

		void Update()
		{
			// Go to title screen
			if (Input.GetKeyDown(KeyCode.Escape))
				ScreenFader.Instance.FadeOut(OnBackToTitleScreen);

#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBGL
			Mouse();
#endif

#if !UNITY_EDITOR && (UNITY_ANDROID || UNITY_IPHONE)
				Touch();
#endif
		}

		#endregion

		#region Private methods

		void Mouse()
		{
			if (Input.GetMouseButtonUp(0))
				ScreenFader.Instance.FadeOut(OnBackToTitleScreen);
		}

		void Touch()
		{
			if (Input.touchCount == 1 && Input.touches[0].phase == TouchPhase.Ended)
				ScreenFader.Instance.FadeOut(OnBackToTitleScreen);
		}

		void WriteScore(int score)
		{
			if (scoreText != null)
				scoreText.text = string.Format(Constants.Texts.GameOverScore, score);
		}

		IEnumerator CO_ShakeCamera()
		{
			yield return new WaitForSeconds(1.45f);

			if (sceneCamera != null)
				sceneCamera.GetComponent<Shaker>().Shake();
		}

		#endregion

		#region Event handlers

		void OnBackToTitleScreen()
		{
			SceneManager.LoadScene(Constants.Scenes.Title);
		}

		#endregion
	}
}
