using UnityEngine;
using UnityEngine.EventSystems;

namespace Konyisoft
{
	public class InputManager : Singleton<InputManager>
	{
		#region Public methods

		public RaycastHit? Raycast(Vector2 position, GameObject go, int layerMask)
		{
			RaycastHit? h = null;
			if (go != null)
			{
				Collider coll = go.GetComponent<Collider>();
				if (coll != null)
				{
					Ray ray = Camera.main.ScreenPointToRay(position);
					RaycastHit hit;
					if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask) && coll == hit.collider)
					{
						h = hit;
					}
				}
			}
			return h;
		}		
		
		public RaycastHit? Raycast(Vector2 position, Component component, int layerMask)
		{
			if (component != null)
				return Raycast(position, component.gameObject, layerMask);
			else
				return null;
		}
		
		public bool IsMouseOverUI()
		{
			return EventSystem.current.IsPointerOverGameObject();
		}

		public bool IsTouchOverUI(int fingerId)
		{
			return EventSystem.current.IsPointerOverGameObject(fingerId);
		}
		
		public bool IsTouchOverAnyUI()
		{
			bool r = false;
			for (int i = 0; i < Input.touchCount; i++)
			{
				if (EventSystem.current.IsPointerOverGameObject(Input.touches[i].fingerId))
				{
					r = true;
					break;
				}
			}
			return r;
		}

		#endregion

		#region Touch handling

		public Touch? GetTouchByFingerId(int fingerId)
		{
			Touch? touch = null;
			for (int i = 0; i < Input.touchCount; i++)
			{
				if (fingerId == Input.touches[i].fingerId)
				{
					touch = Input.touches[i];
					break;
				}
			}
			return touch;
		}

		public bool HasBeganTouch()
		{
			bool r = false;
			for (int i = 0; i < Input.touchCount; i++)
			{
				if (Input.touches[i].phase == TouchPhase.Began)
				{
					r = true;
					break;
				}
			}
			return r;
		}

		public Touch? GetLastBeganTouch()
		{
			Touch? touch = null;
			for (int i = 0; i < Input.touchCount; i++)
			{
				Touch t = Input.touches[i];
				if (t.phase == TouchPhase.Began)
				{
					touch = t;
					break;
				}
			}
			return touch;
		} 		
		
		#endregion
	}
}
