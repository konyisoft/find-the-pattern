using UnityEngine;

namespace Konyisoft
{
	public class ToolsManager : Singleton<ToolsManager>
	{
		#region Fields and propertes
		
		public Clickable clickable;
		public Marker marker;
		public Symbol rightSymbol;
		public Symbol wrongSymbol;

		#endregion
		
		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			
			if (clickable == null)
			{
				Debug.LogError("No clickable object attached.");
				Debug.Break();
			}
			
			if (marker == null)
			{
				Debug.LogError("Marker not attached.");
				Debug.Break();
			}

			if (rightSymbol == null)
			{
				Debug.LogError("Right symbol not attached.");
				Debug.Break();
			}

			if (wrongSymbol == null)
			{
				Debug.LogError("Wrong symbol not attached.");
				Debug.Break();
			}
		}
		
		#endregion
	}
}
