using UnityEngine;

namespace Konyisoft
{
	public class TimerManager : Singleton<TimerManager>
	{
		#region Enumerations

		public enum Direction
		{
			Down = -1,
			Up = 1
		}

		#endregion

		#region Fields

		public Direction direction = Direction.Up;
		public int elapse = 60;
		public float slice = 1.0f;
		public float speed = 1f;
		public bool runAtStart = false;
		public float lerpTime = 0.01f;

		public delegate void OnTimeChanged();
		public delegate void OnTimeTick();
		public delegate void OnTimeElapsed();
		public delegate void OnTimeLerped();

		public OnTimeChanged onTimeChanged;
		public OnTimeTick onTimeTick;
		public OnTimeElapsed onTimeElapsed;
		public OnTimeLerped onTimeLerped;

		bool isRunning = true;
		int time = 0;
		float timeTimer = 0f;
		float lerpTimer = 0f;
		int desiredTime = -1;
		bool isLerping = false;

		#endregion

		#region Properties

		public int Time
		{
			get { return time; }
		}

		public bool IsLerping
		{
			get { return isLerping; }
		}

		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			Set();
		}

		void Start()
		{
			if (runAtStart)
				Run();
		}

		void Update()
		{
			// Increasing or decreasing with lerp
			if (isLerping)
			{
				if (desiredTime != time)
				{
					lerpTimer += UnityEngine.Time.deltaTime;
					if (lerpTimer >= lerpTime)
					{
						if (time < desiredTime)
							time++;
						else
							time--;

						lerpTimer = 0.0f;

						// Call delegate
						if (onTimeChanged != null)
							onTimeChanged();
					}
				}
				// Stop if equals
				if (desiredTime == time)
				{
					isLerping = false;
					ResetValues();
					if (onTimeLerped != null)
						onTimeLerped();
				}

				return; // Important!
			}

			// Counting the time
			if (isRunning)
			{
				timeTimer += UnityEngine.Time.deltaTime * speed;
				if (timeTimer >= slice)
				{
					time += 1 * (int)direction;
					timeTimer -= slice; // Timer restarts with the remaining value
					if (onTimeTick != null) // Call delegate
						onTimeTick();
				}

				// Time is up?
				if ((direction == Direction.Up && time >= elapse) || (direction == Direction.Down && time <= 0))
				{
					isRunning = false;
					if (onTimeElapsed != null) // Call delegate
						onTimeElapsed();
				}
			}
		}

		#endregion

		#region Public methods

		public void Set(int elapse = 0)
		{
			if (elapse > 0f)
				this.elapse = elapse;
			isLerping = false;
			ResetValues();
			time = direction == Direction.Down ? this.elapse : 0;
			if (onTimeChanged != null) // Call delegate
				onTimeChanged();
		}

		public void IncreaseWith(int value, bool withLerp = false)
		{
			if (withLerp)
			{
				desiredTime = time + value;
				lerpTimer = 0f;
				isLerping = true;
			}
			else
			{
				time += value;
				if (onTimeChanged != null) // Call delegate
					onTimeChanged();
			}
		}

		public void DecreaseWith(int value, bool withLerp = false)
		{
			if (withLerp)
			{
				desiredTime = time - value > 0 ? time - value : 0;
				lerpTimer = 0f;
				isLerping = true;
			}
			else
			{
				time = time - value > 0 ? time - value : 0;
				if (onTimeChanged != null) // Call delegate
					onTimeChanged();
			}
		}

		public void Run()
		{
			isRunning = true;
			isLerping = false;
			ResetValues();
		}

		public void Stop(bool reset = false)
		{
			isRunning = false;
			isLerping = false;
			ResetValues();
			if (reset)
				Set();
		}

		public void Pause()
		{
			isRunning = false;
		}

		public void Resume()
		{
			isRunning = true;
		}

		public string GetTimeString()
		{
			int minutes = Mathf.FloorToInt(time / 60);
			int seconds = Mathf.FloorToInt(time - minutes * 60);
			return string.Format(Constants.Texts.Timer, minutes, seconds);
		}

		#endregion

		#region Private methods

		void ResetValues()
		{
			timeTimer = 0f;
			lerpTimer = 0f;
			desiredTime = -1;
		}

		#endregion
	}
}
