using UnityEngine;

namespace Konyisoft
{
	public class UIManager : Singleton<UIManager>
	{
		#region Fields

		public TextMesh timeText;
		public TextMesh timeTextShadow;
		public Transform timeTextAnchor;
		public TextMesh scoreText;
		public TextMesh scoreTextShadow;
		public Transform scoreTextAnchor;
		public UIButton doneButton;
		public Transform doneButtonTextAnchor;
		
		public int timeAlarmAt = 10;
		public Color timeAlarmColor = Color.red;
		public bool showAlarm = true;
		
		Color timeTextOrigColor;

		#endregion

		#region Mono methods
		
		protected override void Awake()
		{
			base.Awake();
			
			if (timeText == null || timeTextShadow == null || scoreText == null || scoreTextShadow == null)
				Debug.LogWarning("Missing UI text element(s).");
				
			if (doneButton == null)
			{
				Debug.LogError("No Done button attached. This is a must have.");
				Debug.Break();
			}
			else
			{
				doneButton.onClick += OnDoneButtonClick;
			}
			
			if (timeText != null)
				timeTextOrigColor = timeText.color;
		}
		
		#endregion
		
		#region Public methods

		public void WriteTime(int time)
		{
			int minutes = Mathf.FloorToInt(time / 60);
			int seconds = Mathf.FloorToInt(time - minutes * 60);
			string text = string.Format(Constants.Texts.Timer, minutes, seconds);

			if (timeText != null)
			{
				timeText.text = text;

				// Coloring text depending on current time
				if (showAlarm)
				{
					if (time <= timeAlarmAt && timeText.color != timeAlarmColor)
						timeText.color = timeAlarmColor;
					else if (time > timeAlarmAt && timeText.color != timeTextOrigColor)
						timeText.color = timeTextOrigColor;
				}
			}

			if (timeTextShadow != null)
				timeTextShadow.text = text;
		}

		public void WriteScore(int score)
		{
			string text = string.Format(Constants.Texts.Score, score);
			
			if (scoreText != null)
				scoreText.text = text;
			if (scoreTextShadow != null)
				scoreTextShadow.text = text;
		}
		
		public void RotateAnchors(float z)
		{
			RotateAnchor(scoreTextAnchor, z);
			RotateAnchor(timeTextAnchor, z);
			RotateAnchor(doneButtonTextAnchor, z);
		}

		#endregion
		
		#region Private methods

		void RotateAnchor(Transform anchor, float z)
		{
			if (anchor != null)
			{
				Vector3 eulerAngles = anchor.eulerAngles;
				eulerAngles.z = z;
				anchor.eulerAngles = eulerAngles;
			}
		}
		
		#endregion
		
		#region Event handlers
		
		void OnDoneButtonClick()
		{
			GameManager.Instance.DoneButtonClick();
		}
		
		#endregion
	}
}
