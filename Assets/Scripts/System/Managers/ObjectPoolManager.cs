using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Konyisoft
{
	public class ObjectPoolManager : Singleton<ObjectPoolManager>
	{
		#region Classes and structs

		[Serializable]
		public class PoolPrefab
		{
			public GameObject prefab;
			public int count = 1;
		}

		public class PoolObject
		{
			public bool Used { get { return used; } }
			public bool Unused { get { return !used; } }
			public GameObject GameObject { get { return gameObject; } }

			private bool used = false;
			private GameObject gameObject;

			public PoolObject(GameObject gameObject)
			{
				this.gameObject = gameObject;
				Unuse();
			}

			public void Use()
			{
				gameObject.SetActive(true);
				used = true;
			}

			public void Unuse()
			{
				gameObject.SetActive(false);
				used = false;
			}
		}

		public class Pool
		{
			public int UsedCount
			{
				get { return objects.FindAll(o => o.Used).Count; }
			}

			public int UnusedCount
			{
				get { return objects.FindAll(o => o.Unused).Count; }
			}

			public int TotalCount
			{
				get { return objects.Count; }
			}

			GameObject parentObject;
			List<PoolObject> objects = new List<PoolObject>();

			public Pool(GameObject parentObject, GameObject prefab, int count)
			{
				this.parentObject = parentObject;
				InstantiatePrefabs(prefab, count);
			}

			void InstantiatePrefabs(GameObject prefab, int count)
			{
				if (prefab != null && count > 0)
				{
					for (int i = 0; i < count; i++)
					{
						InstantiatePrefab(prefab);
					}
				}
			}

			void InstantiatePrefab(GameObject prefab)
			{
				GameObject go = Instantiate(prefab);
				if (parentObject != null)
				{
					go.transform.SetParent(parentObject.transform);
					go.name = parentObject.name.Replace("(Clone)", "");  // Just to be sure
					PoolObject poolObject = new PoolObject(go);
					objects.Add(poolObject);
				}
			}
			
			public bool Contains(GameObject go)
			{
				return objects.Find(o => o.GameObject == go) != null;
			}

			public GameObject Get()
			{
				PoolObject po = objects.Find(o => o.Unused);
				if (po != null)
				{
					po.Use();
					return po.GameObject;
				}
				else
				{
					throw new Exception("Pool has run out of objects.");
				}
			}

			public void Put(GameObject go)
			{
				PoolObject po = objects.Find(o => o.GameObject == go);
				if (po != null)
					po.Unuse();
			}
		}

		#endregion

		#region Fields

		public GameObject instancesParent;
		public List<PoolPrefab> prefabs = new List<PoolPrefab>();
		public bool debugMode = false;

		Dictionary<string, Pool> pools = new Dictionary<string, Pool>();
		
		#endregion

		#region Properties

		public Dictionary<string, Pool> Pools { get { return pools; } }
		
		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			CreatePools();
		}

		#endregion

		#region Public methods

		public GameObject GetObject(string poolId)
		{
			GameObject go = null;
			if (pools.ContainsKey(poolId))
			{
				go = pools[poolId].Get();
			}
			else
			{
				throw new Exception("Unknown pool id: " + poolId);
			}
			return go;
		}

		public void PutObject(GameObject go)
		{
			bool hasPut = false;
			foreach (KeyValuePair<string, Pool> kvp in pools)
			{
				if (go.name == kvp.Key)
				{
					pools[kvp.Key].Put(go);
					hasPut = true;
					break;
				}
			}
			
			if (!hasPut)
				throw new Exception("Object is not from the pool.");
		}

		#endregion

		#region Private methods

		void CreatePools()
		{
			float startTime = Time.realtimeSinceStartup;

			prefabs.ForEach(item =>
			{
				// Create pool parent object
				GameObject poolParent = new GameObject(item.prefab.name);
				poolParent.transform.SetParent(instancesParent == null ? transform : instancesParent.transform);

				// Create a pool
				Pool pool = new Pool(poolParent, item.prefab, item.count);
				pools.Add(item.prefab.name, pool);
			});

			float elapsedTime = Time.realtimeSinceStartup - startTime;

			if (debugMode)
				Debug.Log("Pool creation time: " + string.Format("{0:0.000}", elapsedTime) + " secs.");
		}

		#endregion
	}
}
