using System;
using System.Collections;
using UnityEngine;

namespace Konyisoft
{
	public class TableManager : Singleton<TableManager>
	{
		#region Classes and structs

		[Serializable]
		public class Settings
		{
			[Range(1, 100)]
			public int cols = 25;
			[Range(1, 100)]
			public int rows = 25;
			public Vector3 positionOffset = Vector3.zero;
			public float cellDistance = 1f;
		}

		#endregion

		#region Fields

		public Settings tableSettings = new Settings();
		public Settings patternSettings = new Settings();
		public delegate void OnTableCellsColored();
		public delegate void OnTableCellsFaded();

		CellGenerator generator = null;
		bool isGenerated = false;

		#endregion

		#region Properties

		public Cell[,] Table
		{
			get { return isGenerated ? generator.Table : null; }
		}

		public Cell[,] Pattern
		{
			get { return isGenerated ? generator.Pattern : null; }
		}

		public CellGenerator Generator
		{
			get { return generator; }
		}

		public bool IsGenerated
		{
			get { return isGenerated; }
		}

		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();

			if (patternSettings.cols > tableSettings.cols || patternSettings.cols > tableSettings.cols)
			{
				Debug.LogError("The pattern seems larger than the table. This is not allowed.");
				Debug.Break();
			}
		}

		#endregion

		#region Public methods

		public void Generate(CellGenerator generator)
		{
			// Clears the previous table and pattern
			Clear();
			if (generator != null)
			{
				this.generator = generator;
				// Create cell and pattern
				generator.Generate(tableSettings, patternSettings);
				// Resizing and positioning clickable area
				ResizeAndPositionClickable();
				isGenerated = true;
			}
		}

		public Cell GetNearestCell(Vector2 position)
		{
			Cell c = null;
			if (isGenerated)
			{
				float lastDistance = Mathf.Infinity;
				for (int col = 0; col < tableSettings.cols; col++)
				{
					for (int row = 0; row < tableSettings.rows; row++)
					{
						Cell cell = generator.Table[col, row];
						Vector2 cellLeftTopPosition = generator.GetCellLeftTopPosition(cell);
						float distance = Vector2.Distance(position, cellLeftTopPosition);
						if (distance < lastDistance)
						{
							lastDistance = distance;
							c = cell;
						}
					}
				}
			}
			return c;
		}

		public bool PatternEquals(Cell cell)
		{
			return cell != null ? PatternEquals(cell.Col, cell.Row) : false;
		}

		public bool PatternEquals(int baseCol, int baseRow)
		{
			bool r = true;
			try
			{
				for (int col = 0; col < patternSettings.cols; col++)
				{
					for (int row = 0; row < patternSettings.rows; row++)
					{
						Cell patternCell = generator.Pattern[col, row];
						Cell tableCell = generator.Table[baseCol + col, baseRow + row];
						if (patternCell.Id != tableCell.Id)
						{
							r = false;
							break;
						}
					}

					if (!r)
						break;
				}
			}
			catch
			{
				r = false; // On any error it returns false
			}

			return r;
		}

		public void ColorizeCells(float multiplier, float speed, OnTableCellsColored callback = null)
		{
			// Colorize the whole table except the pattern
			StopAllCoroutines();
			StartCoroutine(CO_LerpCellsColor(multiplier, speed, callback));
		}

		public void FadeCells(float value, float speed, OnTableCellsFaded callback = null)
		{
			// Fades the whole table except the pattern
			StopAllCoroutines();
			StartCoroutine(CO_LerpCellsAlpha(value, speed, callback));
		}

		#endregion

		#region Private methods

		void Clear()
		{
			StopAllCoroutines();
			if (isGenerated)
			{
				generator.Clear();
				generator = null;
			}
			isGenerated = false;
		}

		IEnumerator CO_LerpCellsColor(float multiplier, float speed, OnTableCellsColored callback = null)
		{
			Color[,] targetColors = GetCellsColor();
			targetColors = ModifyColors(targetColors, tableSettings.cols, tableSettings.rows, false, multiplier);
			bool done = false;
			float startTime = Time.time;
			while (!done && Time.time - startTime < 3f) // Security condition (max 3 secs)
			{
				done = LerpCellsColor(targetColors, speed);
				yield return new WaitForEndOfFrame();
			}

			if (callback != null)
				callback();

			yield return null;
		}

		IEnumerator CO_LerpCellsAlpha(float value, float speed, OnTableCellsFaded callback = null)
		{
			bool done = false;
			float startTime = Time.time;
			while (!done && Time.time - startTime < 3f) // Security condition (max 3 secs)
			{
				done = LerpCellsAlpha(value, speed);
				yield return new WaitForEndOfFrame();
			}

			if (callback != null)
				callback();

			yield return null;
		}

		bool LerpCellsColor(Color[,] targetColors, float speed)
		{
			bool r = true; // Default is true (=done)
			for (int col = 0; col < tableSettings.cols; col++)
			{
				for (int row = 0; row < tableSettings.rows; row++)
				{
					Cell tableCell = generator.Table[col, row];

					if (tableCell != null && tableCell.HasMaterial && !generator.InsideRandomPattern(col, row))
					{
						Color cellColor = tableCell.Material.color;
						cellColor = Color.Lerp(cellColor, targetColors[col, row], Time.deltaTime * speed);
						tableCell.Material.color = cellColor;

						if (!ColorsApproximately(cellColor, targetColors[col, row]))
						{
							r = false;  // Not done yet
						}
						else if (cellColor != targetColors[col, row])
						{
							cellColor = targetColors[col, row];
							tableCell.Material.color = cellColor;
						}
					}
				}
			}
			return r;
		}

		bool LerpCellsAlpha(float targetAlpha, float speed)
		{
			bool r = true; // Default is true (=done)
			for (int col = 0; col < tableSettings.cols; col++)
			{
				for (int row = 0; row < tableSettings.rows; row++)
				{
					Cell tableCell = generator.Table[col, row];
					if (tableCell != null && !generator.InsideRandomPattern(col, row))
					{
						// Main material's alpha
						if (tableCell.HasMaterial)
						{
							Color cellColor = tableCell.Material.color;
							cellColor.a = Mathf.MoveTowards(cellColor.a, targetAlpha, Time.deltaTime * speed);
							tableCell.Material.color = cellColor;

							if (Mathf.Abs(cellColor.a - targetAlpha) > 0.025f)
							{
								r = false; // Not done yet
							}
							else if (cellColor.a != targetAlpha)
							{
								cellColor.a = targetAlpha;
								tableCell.Material.color = cellColor;
							}
						}

						// Also the shadow
						if (tableCell.HasShadow)
						{
							Color shadowColor = generator.Table[col, row].ShadowMaterial.color;
							shadowColor.a = Mathf.MoveTowards(shadowColor.a, targetAlpha, Time.deltaTime * speed);
							tableCell.ShadowMaterial.color = shadowColor;

							if (Mathf.Abs(shadowColor.a - targetAlpha) > 0.025f)
							{
								r = false; // Not done yet
							}
							else if (shadowColor.a != targetAlpha)
							{
								shadowColor.a = targetAlpha;
								tableCell.ShadowMaterial.color = shadowColor;
							}
						}
					}
				}
			}
			return r;
		}

		Color[,] GetCellsColor()
		{
			Color[,] colors = new Color[tableSettings.cols, tableSettings.rows];
			for (int col = 0; col < tableSettings.cols; col++)
			{
				for (int row = 0; row < tableSettings.rows; row++)
				{
					if (generator.Table[col, row].HasMaterial)
						colors[col, row] = generator.Table[col, row].Material.color;
				}
			}
			return colors;
		}

		Color[,] ModifyColors(Color[,] colors, int cols, int rows, bool createGrayScale = false, float multiplier = 1f)
		{
			for (int col = 0; col < cols; col++)
			{
				for (int row = 0; row < rows; row++)
				{
					if (createGrayScale)
					{
						float grayValue = colors[col, row].grayscale;
						colors[col, row] = new Color(grayValue, grayValue, grayValue, colors[col, row].a);
					}
					colors[col, row].r = Mathf.Clamp(colors[col, row].r * multiplier, 0f, 1f);
					colors[col, row].g = Mathf.Clamp(colors[col, row].g * multiplier, 0f, 1f);
					colors[col, row].b = Mathf.Clamp(colors[col, row].b * multiplier, 0f, 1f);
				}
			}
			return colors;
		}

		bool ColorsApproximately(Color c1, Color c2)
		{
			return
				Mathf.Abs(c1.r - c2.r) < 0.01 &&
				Mathf.Abs(c1.g - c2.g) < 0.01 &&
				Mathf.Abs(c1.b - c2.b) < 0.01;
		}

		void ResizeAndPositionClickable()
		{
			// Resizing it large enough to fill the whole screen
			ToolsManager.Instance.clickable.SetSize(new Vector2(80f, 40f));
			// Positioning
			ToolsManager.Instance.clickable.SetPosition(new Vector2(tableSettings.positionOffset.x, tableSettings.positionOffset.y));
		}

		#endregion
	}
}