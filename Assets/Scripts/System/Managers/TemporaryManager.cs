using UnityEngine;
using System.Collections.Generic;

namespace Konyisoft
{
	public class TemporaryManager : Singleton<TemporaryManager>
	{
		#region Properties

		public GameObject TemporaryObject
		{
			get { return temporaryObject; }
		}

		#endregion

		#region Fields
		
		List<GameObject> objects = new List<GameObject>();
		GameObject temporaryObject;

		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			temporaryObject = GameObject.Find(Constants.Objects.Temporary);
			// Create one if not found
			if (temporaryObject == null)
			{
				GameObject systemObject = GameObject.Find(Constants.Objects.System);
				temporaryObject = new GameObject(Constants.ObjectNames.Temporary);
				// Parenting if System exists
				if (systemObject != null)
					temporaryObject.transform.parent = systemObject.transform;
			}
		}

		#endregion

		#region Public methods

		public void Add(GameObject go)
		{
			if (go != null && !objects.Contains(go))
			{
				objects.Add(go);
				go.transform.parent = temporaryObject.transform;
			}
		}

		public void Add(Component component)
		{
			if (component != null)
				Add(component.gameObject);
		}

		public void Remove(GameObject go, bool destroyImmediate = false)
		{
			if (go != null && objects.Contains(go))
			{
				objects.Remove(go);
				if (destroyImmediate)
					DestroyImmediate(go);
				else
					Destroy(go);
			}
		}

		public void Remove(Component component, bool destroyImmediate = false)
		{
			if (component != null)
				Remove(component.gameObject, destroyImmediate);
		}

		public void Clear()
		{
			for (int i = objects.Count - 1; i >= 0; i++)
			{
				Destroy(objects[i]);
			}
			objects.Clear();
		}

		#endregion
	}
}
