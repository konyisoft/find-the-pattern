using UnityEngine;

namespace Konyisoft
{
	public class TitleMover : GameBehaviour
	{
		#region Fields
		
		public float speedX = 1f;
		public float minX = 0f;
		public float maxX = 0f;
		public float speedY = 1f;
		public float minY = 0f;
		public float maxY = 0f;
		public bool isMoving = true;
		
		Vector3 origPosition;
		
		#endregion
		
		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			origPosition = Transform.position;
			Default();
		}
		
		void Update()
		{
			if (isMoving)
			{	
				float x = Mathf.SmoothStep(minX, maxX, Mathf.PingPong(Time.time * speedX, 1f));
				float y = Mathf.SmoothStep(minY, maxY, Mathf.PingPong(Time.time * speedY, 1f));
				Transform.position = new Vector3(x, y, Transform.position.z);
			}
		}
		
		#endregion
		
		#region Public methods
	
		public void Default()
		{
			Transform.position = new Vector3(minX, minY, origPosition.z);
		}
	
		#endregion
	}
}