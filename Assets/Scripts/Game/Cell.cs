using UnityEngine;

namespace Konyisoft
{
	public class Cell : GameBehaviour
	{
		#region Fields

		CellShadow shadow; // Shadow object

		#endregion

		#region Properties

		public int Col
		{
			get; set;
		}

		public int Row
		{
			get; set;
		}

		public string Id
		{
			get; set;
		}

		public Vector3 Position
		{
			get { return Transform.position; }
		}

		public Vector2 Position2D
		{
			get { return Utils.Vector2D(Position); }
		}

		public CellShadow Shadow
		{
			get { return shadow; }
		}

		public bool HasShadow
		{
			get { return shadow != null; }
		}

		public Material Material
		{
			get { return GetMaterial(); }
			set { SetMaterial(value); }
		}

		public Material ShadowMaterial
		{
			get { return GetShadowMaterial(); }
			set { SetShadowMaterial(value); }
		}

		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();

			shadow = gameObject.GetComponentInChildren<CellShadow>();

			// Default values
			Col = 0;
			Row = 0;
			gameObject.name = gameObject.name.Replace("(Clone)", "");
			Id = gameObject.name;
		}

		#endregion

		#region Public methods

		public void AddHashToId(object o)
		{
			// Adds a hash code to the id
			Id += " (" + o.GetHashCode().ToString() + ")";
		}

		public void ResetObject()
		{
			// Reset position and rotation
			gameObject.transform.position = Vector3.zero;
			gameObject.transform.localPosition = Vector3.zero;
			gameObject.transform.rotation = Quaternion.identity;
			gameObject.transform.localRotation = Quaternion.identity;

			// Destroy material
			if (HasMaterial)
				Destroy(Renderer.material);

			// Destroy shadows material
			if (HasShadow && Shadow.HasMaterial)
				Destroy(Shadow.Renderer.material);
		}


		#endregion

		#region Private method

		void SetMaterial(Material material)
		{
			if (HasRenderer)
			{
				if (HasMaterial)
					DestroyImmediate(Renderer.material);
				Renderer.material = material;
			}
		}

		Material GetMaterial()
		{
			return HasMaterial ? Renderer.material : null;
		}

		void SetShadowMaterial(Material material)
		{
			if (HasShadow && Shadow.HasRenderer)
			{
				if (Shadow.HasMaterial)
					DestroyImmediate(Shadow.Renderer.material);
				Shadow.Renderer.material = material;
			}
		}

		Material GetShadowMaterial()
		{
			return HasShadow && Shadow.HasMaterial ? Shadow.Renderer.material : null;
		}

		#endregion
	}
}