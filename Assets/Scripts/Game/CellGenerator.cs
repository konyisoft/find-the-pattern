using System;
using System.Collections.Generic;
using UnityEngine;

namespace Konyisoft
{
	public class CellGenerator : MonoBehaviour
	{
		#region Classes and structs

		[System.Serializable]
		public class GeneratorItem
		{
			public string id = string.Empty;
			public Material material;
			public Material shadowMaterial;
			public int odds = 1;
		}

		#endregion

		#region Fields

		public List<GeneratorItem> items = new List<GeneratorItem>();

		Cell[,] table;
		Cell[,] pattern;
		TableManager.Settings tableSettings;
		TableManager.Settings patternSettings;
		int randomCol = -1;
		int randomRow = -1;

		#endregion

		#region Properties

		public Cell[,] Table
		{
			get { return table; }
		}

		public Cell[,] Pattern
		{
			get { return pattern; }
		}

		public int RandomCol
		{
			get { return randomCol; }
		}

		public int RandomRow
		{
			get { return randomRow; }
		}

		#endregion

		#region Public methods

		public void Generate(TableManager.Settings tableSettings, TableManager.Settings patternSettings)
		{
			this.tableSettings = tableSettings;
			this.patternSettings = patternSettings;

			// Generate cell arrays based on the given settings
			table = new Cell[tableSettings.cols, tableSettings.rows];
			pattern = new Cell[patternSettings.cols, patternSettings.rows];

			// Get a random position in table (for pattern)
			int maxCols = tableSettings.cols - patternSettings.cols;
			int maxRows = tableSettings.rows - patternSettings.rows;

			// 0,0 value is not allowed, it's the start position of the marker
			randomCol = 0;
			randomRow = 0;
			while (randomCol == 0 && randomRow == 0)
			{
				randomCol = UnityEngine.Random.Range(0, maxCols);
				randomRow = UnityEngine.Random.Range(0, maxRows);
			}

			// TODO: delete it
			// Debug.Log(randomCol + " - " + randomRow);

			// Checking prefab list
			if (CheckItems())
			{
				List<int> oddsIndexes = GetOddsIndexes();

				// Preprocessing all cells
				OnBeforeGenerate(table, pattern);

				for (int col = 0; col < tableSettings.cols; col++)
				{
					for (int row = 0; row < tableSettings.rows; row++)
					{
						// Get a random index from the odds list
						int randomIndex = oddsIndexes[UnityEngine.Random.Range(0, oddsIndexes.Count)];
						GeneratorItem randomItem = items[randomIndex];
						Cell tableCell = GenerateCell(randomItem, true);
						if (tableCell != null)
						{
							tableCell.Col = col;
							tableCell.Row = row;
							tableCell.AddHashToId(randomIndex);
							// Processing current cell
							OnCellGenerate(tableCell);
							table[tableCell.Col, tableCell.Row] = tableCell; // Add to array
						}

						// Create pattern cell if needed
						if (InsideRandomPattern(col, row))
						{
							Cell patternCell = GenerateCell(randomItem, false);
							if (patternCell != null)
							{
								// Cloning the current table cell
								patternCell.Col = col - randomCol;
								patternCell.Row = row - randomRow;
								patternCell.Material = tableCell.Material;
								patternCell.ShadowMaterial = tableCell.ShadowMaterial;
								patternCell.Id = tableCell.Id;
								pattern[patternCell.Col, patternCell.Row] = patternCell; // Add to array
							}
						}
					}
				}

				// Set cell positions
				PositionCells(table, tableSettings);
				PositionCells(pattern, patternSettings);

				// Postprocessing all cells
				OnAfterGenerate(table, pattern);

				// Clear odds
				oddsIndexes.Clear();
			}
		}

		public void Clear()
		{
			OnBeforeClear(table, pattern);

			if (tableSettings != null)
				Clear(table, tableSettings);
			if (patternSettings != null)
				Clear(pattern, patternSettings);

			OnAfterClear(table, pattern);

			// Zero/null values
			table = null;
			pattern = null;
			tableSettings = null;
			patternSettings = null;
			randomCol = -1;
			randomRow = -1;
		}

		public bool InsideRandomPattern(int col, int row)
		{
			return
				patternSettings != null &&
				col >= randomCol &&
				row >= randomRow &&
				col < randomCol + patternSettings.cols &&
				row < randomRow + patternSettings.rows;
		}

		public Vector2 GetCellLeftTopPosition(Cell cell)
		{
			if (cell == null)
				return Vector2.zero;

			return new Vector2(
				cell.Position.x - (tableSettings.cellDistance * 0.5f),
				cell.Position.y + (tableSettings.cellDistance * 0.5f)
			);
		}

		public Vector2 GetCellRightBottomPosition(Cell cell)
		{
			if (cell == null)
				return Vector2.zero;

			return new Vector2(
				cell.Position.x + (tableSettings.cellDistance * 0.5f),
				cell.Position.y - (tableSettings.cellDistance * 0.5f)
			);
		}

		#endregion

		#region Event methods

		protected virtual void OnBeforeGenerate(Cell[,] table, Cell[,] pattern)
		{
		}

		protected virtual void OnAfterGenerate(Cell[,] table, Cell[,] pattern)
		{
		}

		protected virtual void OnCellGenerate(Cell cell)
		{
		}

		protected virtual void OnBeforeClear(Cell[,] table, Cell[,] pattern)
		{
		}

		protected virtual void OnAfterClear(Cell[,] table, Cell[,] pattern)
		{
		}

		protected virtual void OnCellClear(Cell cell)
		{
		}

		#endregion

		#region Private methods

		bool CheckItems()
		{
			if (items.Count < 1)
			{
				Debug.LogError("At least one generator item is required to generate a cells array.");
				Debug.Break();
				return false;
			}
			return true;
		}

		Cell GenerateCell(GeneratorItem item, bool setMaterial, Vector3 position = default(Vector3), Quaternion rotation = default(Quaternion))
		{
			Cell cell = null;
			if (item != null && !string.IsNullOrEmpty(item.id))
			{
				GameObject go = ObjectPoolManager.Instance.GetObject(item.id);
				go.transform.position = position;
				go.transform.rotation = rotation;
				cell = go.GetComponent<Cell>();
				if (setMaterial && cell != null)
				{
					if (item.material != null)
						cell.Material = item.material;
					if (item.shadowMaterial != null)
						cell.ShadowMaterial = item.shadowMaterial;
				}
			}
			return cell;
		}

		void PositionCells(Cell[,] cells, TableManager.Settings settings)
		{
			Vector3 startPosition = GetCenteredPosition(settings.cols, settings.rows, settings.cellDistance);
			startPosition += settings.positionOffset; // Add offset
			Vector3 position = startPosition;

			for (int col = 0; col < settings.cols; col++)
			{
				for (int row = 0; row < settings.rows; row++)
				{
					Cell cell = cells[col, row];
					if (cell != null)
						cell.Transform.position = position;
					position.y -= settings.cellDistance;
				}
				position.y = startPosition.y;
				position.x += settings.cellDistance;
			}
		}

		void Clear(Cell[,] cells, TableManager.Settings settings)
		{
			// Destroys the complete cells array
			if (cells != null)
			{
				for (int col = 0; col < settings.cols; col++)
				{
					for (int row = 0; row < settings.rows; row++)
					{
						Cell cell = cells[col, row];
						if (cell != null)
						{
							cell.ResetObject();
							OnCellClear(cell);
							ObjectPoolManager.Instance.PutObject(cell.gameObject);
						}
					}
				}
				Array.Clear(cells, 0, cells.Length);
			}
		}

		List<int> GetOddsIndexes()
		{
			// Collect indexes to a list based on items' odds
			List<int> oddsIndexes = new List<int>();
			for (int i = 0; i < items.Count; i++)
			{
				GeneratorItem item = items[i];
				if (item != null)
				{
					for (int o = 0; o < item.odds; o++)
					{
						oddsIndexes.Add(i);  // Add item's index to the oddsIndexes list
					}
				}
			}
			return oddsIndexes;
		}

		Vector3 GetCenteredPosition(int cols, int rows, float distance)
		{
			return new Vector3(-(distance * (cols - 1)) / 2f, (distance * (rows - 1)) / 2f, 0f);
		}

		#endregion
	}
}