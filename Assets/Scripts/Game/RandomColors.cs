using UnityEngine;
using System.Collections.Generic;

namespace Konyisoft
{
	public class RandomColors : CellGenerator
	{
		#region Fields

		public List<Color> colors = new List<Color>();

		List<Material> cache = new List<Material>();

		#endregion

		#region Protected methods

		protected override void OnBeforeGenerate(Cell[,] table, Cell[,] pattern)
		{
			base.OnBeforeGenerate(table, pattern);
			if (colors.Count < 1)
			{
				Debug.LogError("At least one color is required to generate a RandomColor table");
				Debug.Break();
			}
		}

		protected override void OnCellGenerate(Cell cell)
		{
			base.OnCellGenerate(cell);
			// Sets a random color if the cell has got a material
			if (cell.Material != null)
			{
				// Get a random color
				Color color = colors[Random.Range(0, colors.Count)];
				//Try to get the material from cache
				Material material = GetFromCache(cell.Material.name, color);
				// Create and add to cache if not found
				if (material == null)
				{
					material = CloneMaterial(cell.Material, color);
					cache.Add(material);
				}

				cell.Material = material;
				cell.AddHashToId(color);
			}
		}

		#endregion

		#region Private methods

		Material CloneMaterial(Material material, Color color)
		{
			Material item = new Material(material); // Create a copy
			item.color = color;
			return item;
		}

		Material GetFromCache(string name, Color color)
		{
			Material m = null;
			for (int i = 0; i < cache.Count; i++)
			{
				Material item = cache[i];
				if (name == item.name && color == item.color)
				{
					m = item;
					break;
				}
			}
			return m;
		}

		#endregion
	}
}

