namespace Konyisoft
{
	public static class Globals
	{
		public static class GameValues
		{
			public static int Score = 0;
			public static int Time = 0;
			public static int LevelIndex = 1;
			public static int TotalTime = 0;
			
			public static void Reset()
			{
				Score = 0;
				Time = 0;
				LevelIndex = 1;
				TotalTime = 0;
			}
		}
	}
}

