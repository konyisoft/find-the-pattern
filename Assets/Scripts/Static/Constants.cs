using System.Collections.Generic;

namespace Konyisoft
{
	public class Constants 
	{
		public class Scenes
		{
			public const string Title = "Title";
			public const string Game = "Game";
			public const string Intermediate = "Intermediate";
			public const string GameOver = "GameOver";
			public const string End = "End";
		}

		public class Layers
		{
			public const int Levels = 8;
			public const int Tools = 9;
			public const int Background = 10;
			public const int Scene = 11;			
		}

		public class LayerMasks
		{
			public const int Levels = 1 << Layers.Levels;
			public const int Tools = 1 << Layers.Tools;
			public const int Background = 1 << Layers.Background;
			public const int Scene = 1 << Layers.Scene;			
		}
		
		public class Objects
		{
			public const string System      = "/[System]";
			public const string Managers    = System + "/Managers";
			public const string Temporary   = System + "/Temporary";
			
			public const string Levels      = "/Levels";
			public const string Tools       = "/Tools";
		}
		
		public class ObjectNames
		{
			public const string Temporary   = "Temporary";
		}
		
		public class Texts
		{
			public const string Score             = "{0}";
			public const string Timer             = "{0:00}:{1:00}";
			public const string IntermediateScore = "SCORE:  <color=#FFEA48FF>{0}</color>";
			public const string GameOverScore     = "YOUR FINAL SCORE:  <color=#FFEA48FF>{0}</color>";
			public const string EndScoreAndTime   = "YOUR FINAL SCORE:  <color=#FFEA48FF>{0}</color>\nTOTAL TIME:  <color=#FFEA48FF>{1:00}:{2:00}</color>";
			public const string IntermediateLevel = "LEVEL {0}";
		}
		
		public static List<string> LevelTexts = new List<string>()
		{
			/*01*/ "Let's start with some colors",
			/*02*/ "Some more colors",
			/*03*/ "Two colors...",
			/*04*/ "One color...",
			/*05*/ "No color :(",
			/*06*/ "Boring? OK. Let's see some... ummm... COLORS!",
			/*07*/ "Somewhat easier when they are outlined",
			/*08*/ "Do you like dots?",
			/*09*/ "Dots are cool in green",
			/*10*/ "A piece of retro",
			/*11*/ "And now something different",
			/*12*/ "Another one in white",
			/*14*/ "Basic shapes",
			/*14*/ "Triangles",
			/*15*/ "Entering the binary system",
			/*16*/ "More zeros and ones",
			/*17*/ "3^2 + 4^2 + 5^2 Shades of Grey",
			/*18*/ "Stones",
			/*19*/ "Nano droidz",
			/*20*/ "Keep smileying",
		};
	}
}

