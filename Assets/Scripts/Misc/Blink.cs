using UnityEngine;

public class Blink : MonoBehaviour
{
	public float blinkTime = 1.0f;	
	
	Renderer _renderer;
	float timer = 0.0f;

	void Awake()
	{
		_renderer = GetComponent<Renderer>();
	}
	
	void Update()
	{
		if (_renderer != null)
		{
			timer += Time.deltaTime;
			if (timer >= blinkTime)
			{
				_renderer.enabled = !_renderer.enabled;
				timer = 0.0f;
			}
		}
	}
}
