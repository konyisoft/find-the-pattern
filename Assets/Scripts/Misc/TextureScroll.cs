using UnityEngine;

public class TextureScroll : MonoBehaviour
{
	public enum Direction
	{
		Horizontal,
		Vertical
	}
	
	public Direction direction = Direction.Horizontal;
	public float speed = 1.0f;
	
	Renderer _renderer;

	void Awake()
	{
		_renderer = GetComponent<Renderer>();
	}
	
	void Update()
	{
		if (_renderer != null && _renderer.material != null)
		{
			Vector2 offset = _renderer.material.GetTextureOffset("_MainTex");
			switch (direction) 
			{
				case Direction.Horizontal :
					offset.x += Time.deltaTime * speed;
					break;
				case Direction.Vertical :
					offset.y += Time.deltaTime * speed;
					break;
			}
			_renderer.material.SetTextureOffset("_MainTex", offset);
		}
	}
}
