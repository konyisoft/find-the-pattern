using UnityEngine;

public class HandHeld : MonoBehaviour
{
	public Vector3 range = new Vector3(0.7f, 0.7f, 0f);
	public float speed = 1f;

	Vector3 origPosition = Vector3.zero;
	Vector3 desiredPosition = Vector3.zero;
	Transform _transform;
	
	void Awake()
	{
		_transform = GetComponent<Transform>();
		origPosition = _transform.position;
		SetDesiredPosition();
	}
	
	void Update()
	{
		_transform.position = Vector3.MoveTowards(_transform.position, desiredPosition, Time.deltaTime * speed);
		
		if (Vector3.Distance(_transform.position, desiredPosition) < 0.01f)
		{
			_transform.position = desiredPosition;
			SetDesiredPosition();
		}
	}
	
	private void SetDesiredPosition()
	{
		Vector3 randomPosition = new Vector3(
			Random.Range(-range.x, range.x),
			Random.Range(-range.y, range.y),
			Random.Range(-range.z, range.z)
		);
		
		desiredPosition = origPosition + randomPosition;
	}
}
