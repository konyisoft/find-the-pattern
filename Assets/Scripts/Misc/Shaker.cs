using UnityEngine;

public class Shaker : MonoBehaviour
{
	public float intensity = 0.3f;
	public float decay = 0.002f;

	Vector3 origPosition = Vector3.zero;
	Quaternion origRotation = Quaternion.identity;
	float currentIntensity = 0.05f;
	bool isShaking = false;
	Transform _transform;
	
	void Awake()
	{
		_transform = GetComponent<Transform>();
		origPosition = _transform.localPosition;
		origRotation = _transform.localRotation;
	}
	
	void Update()
	{
		if (isShaking)
		{
			if (currentIntensity > 0f)
			{
				_transform.localPosition = origPosition + Random.insideUnitSphere * currentIntensity;
				_transform.localRotation = new Quaternion(
					origRotation.x + Random.Range(-currentIntensity, currentIntensity) * 0.2f,
					origRotation.y + Random.Range(-currentIntensity, currentIntensity) * 0.2f,
					origRotation.z + Random.Range(-currentIntensity, currentIntensity) * 0.2f,
					origRotation.w + Random.Range(-currentIntensity, currentIntensity) * 0.2f
				);
				currentIntensity -= decay * Time.deltaTime;
			}
	
			if (currentIntensity <= 0)
			{
				_transform.localPosition = origPosition;
				_transform.localRotation = origRotation;
				isShaking = false;
			}		
		}
	}
 
	public void Shake()
	{
		currentIntensity = intensity;
		isShaking = true;
	}
}
