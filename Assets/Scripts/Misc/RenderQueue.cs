using UnityEngine;

public class RenderQueue : MonoBehaviour
{
	public int renderQueue = 3000;

	void Awake()
	{
		Renderer renderer = GetComponent<Renderer>();
		if (renderer != null && renderer.material != null)
		{
			renderer.material.renderQueue = renderQueue;
		}
	}
}
