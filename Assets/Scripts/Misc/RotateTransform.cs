using UnityEngine;

public class RotateTransform : MonoBehaviour
{
	public Vector3 speed = Vector3.zero;
	Transform _transform;
	
	void Awake()
	{
		_transform = GetComponent<Transform>();
	}

	void Update()
	{
		_transform.Rotate(speed * Time.deltaTime);
	}
}
