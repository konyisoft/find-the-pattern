using UnityEngine;

public class TextureAnimator : MonoBehaviour
{
	[System.Serializable]
	public class AnimatedTexture
	{
		public Texture2D texture;
		public float delay = 1f;
	}
	
	public AnimatedTexture[] textures = new AnimatedTexture[0];
	
	Renderer _renderer;
	float timer = 0.0f;	
	int index = 0;
	
	void Awake()
	{
		_renderer = GetComponent<Renderer>();
		
		if (textures.Length > 0)
		{
			SetTexture(textures[index].texture);
		}
	}
	
	void Update()
	{
		if (textures.Length > 0) 
		{
			timer += Time.deltaTime;
			
			if (timer >= textures[index].delay) 
			{
				if (index < textures.Length - 1)
				{
					index++;
				}
				else
				{
					index = 0;
				}
				SetTexture(textures[index].texture);
				timer = 0f;
			}
		}
	}
	
	void SetTexture(Texture2D texture)
	{
		if (_renderer != null&& _renderer.material != null && texture != null)
		{
			_renderer.material.mainTexture = texture;
		}
	}	
}
