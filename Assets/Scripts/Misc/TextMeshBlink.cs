using UnityEngine;

public class TextMeshBlink : MonoBehaviour
{
	public float speed = 1f;	
	[Range(0f, 1f)]
	public float minAlpha = 0f;
	[Range(0f, 1f)]
	public float maxAlpha = 1f;
	
	TextMesh textMesh;
	Color origColor;

	void Awake()
	{
		textMesh = GetComponent<TextMesh>();
		if (textMesh != null)
			origColor = textMesh.color;
	}
	
	void Update()
	{
		if (textMesh != null)
		{
			Color color = textMesh.color;
			color.a = minAlpha + Mathf.PingPong(Time.time * speed, maxAlpha - minAlpha);
			textMesh.color = color;
		}
	}
	
	public void Default()
	{
		if (textMesh != null)
			textMesh.color = origColor;
	}
}
